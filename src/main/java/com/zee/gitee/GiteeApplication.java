package com.zee.gitee;

import com.zee.gitee.service.CustomOAuth2UserService;
import com.zee.gitee.user.GiteeOAuth2User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.core.user.OAuth2User;

@SpringBootApplication
public class GiteeApplication {

    public static void main(String[] args) {
        SpringApplication.run(GiteeApplication.class, args);
    }

    @EnableWebSecurity
    class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .authorizeRequests()
                    .anyRequest()
                    .authenticated()
                    .and()
                    .oauth2Login()
                    .userInfoEndpoint()
                        .userService(new CustomOAuth2UserService());
            ;
        }
    }
}
