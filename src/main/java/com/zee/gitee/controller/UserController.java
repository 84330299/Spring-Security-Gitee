package com.zee.gitee.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用来演示数据信息
 *
 * @author zhaibolun
 * @version V1.0
 * @Title: Spring Security Gitee
 * @Package com.zee.gitee.controller
 * @Description: TODO
 * @date 2018/02月/08日/ 17:16
 */
@RestController
public class UserController {

    @GetMapping("/user")
    public Object getUser() {
        OAuth2AuthenticationToken authentication = (OAuth2AuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        OAuth2User principal = authentication.getPrincipal();
        return principal.getAttributes();
    }

}
