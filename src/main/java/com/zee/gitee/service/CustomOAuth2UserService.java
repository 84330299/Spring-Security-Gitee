package com.zee.gitee.service;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.zee.gitee.user.GiteeOAuth2User;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;

/**
 * @author zhaibolun
 * @version V1.0
 * @Title: Spring Security Gitee
 * @Package com.zee.gitee.service
 * @Description: TODO
 * @date 2018/02月/08日/ 16:45
 */
public class CustomOAuth2UserService implements OAuth2UserService<OAuth2UserRequest, OAuth2User> {
    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        String uri = userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUri();
        String tokenValue = userRequest.getAccessToken().getTokenValue();
        uri = uri + "?access_token=" + tokenValue;
        String result = HttpUtil.get(uri);
        GiteeOAuth2User giteeOAuth2User = JSONObject.parseObject(result, GiteeOAuth2User.class);
        return giteeOAuth2User;
    }
}
